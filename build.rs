fn main() -> Result<(), Box<dyn std::error::Error>> {   
    tonic_build::configure()
        .build_server(false)
        .type_attribute("MapInstance", "#[derive(serde::Deserialize, serde::Serialize)]")
        .type_attribute("MapRoom", "#[derive(serde::Deserialize, serde::Serialize)]")
        .compile(
            &["proto/map.proto"],
            &["proto"],
        )?;
    Ok(())
}