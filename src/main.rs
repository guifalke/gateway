use actix_web::{get, App, HttpServer, Responder};
use actix_cors::Cors;

use log::debug;
use log::LevelFilter;

use log4rs::config::{Appender, Root};
use log4rs::Config;
use log4rs::append::console::ConsoleAppender;

mod map_module;

use crate::map_module::handler::{create_map, get_by_id};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let stdout = ConsoleAppender::builder().build();

    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .build(Root::builder().appender("stdout").build(LevelFilter::Debug))
        .unwrap();
    
    let _ = log4rs::init_config(config).unwrap();
    debug!("Starting gateway");    

    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
            .wrap(cors)
            .service(index)
            .service(create_map)
            .service(get_by_id)
    })
    .bind(("0.0.0.0", 80))?
    .run()
    .await
}

#[get("/")]
async fn index() -> impl Responder {
    "Hello, World!"
}