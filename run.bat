docker rm gateway -f
docker rmi gateway -f
docker build . -t gateway
docker run --name gateway -d -p 40040:80 gateway
docker network connect dockerNetwork gateway
docker network connect dockerNetwork map_service
