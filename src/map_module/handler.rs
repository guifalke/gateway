use actix_web::{get, web, Responder};

use log::debug;

use crate::map_module::map_server::MapInstance;
use crate::map_module::map_client::{
    create,
    get_map,
};

#[get("/map")]
pub async fn create_map() -> impl Responder {
    debug!("Calling create_map");
    let map_instance: MapInstance = create(30, 30).await.unwrap();
    return web::Json(map_instance);
}

#[get("/map/{id}")]
pub async fn get_by_id(path: web::Path<(String,)>) -> impl Responder {
    debug!("Calling get_map");
    let arg = path.into_inner();
    let map_instance: MapInstance = get_map(arg.0).await.unwrap();
    return web::Json(map_instance);
}