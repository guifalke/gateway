use tonic::{Request, Response};
use std::error::Error;
use std::env;

use log::debug;

use crate::map_module::map_server::map_client::MapClient;
use crate::map_module::map_server::{MapInstance, CreateRequest, GetRequest};

fn get_url() -> String{
    return match env::var_os("SERVICE_URL") {
        Some(v) => v.into_string().unwrap(),
        None => "[::1]".to_owned(),
    };
}

pub async fn create(x_w: i32, y_h: i32) -> Result<MapInstance, Box<dyn Error>> {
	debug!("Requested create map.");

    let url = get_url();

    debug!("The connection url is {}.", url);
    let url_formated: String = format!("http://{}:50051", url); 
    let mut client = MapClient::connect(url_formated).await?;

    let request: Request<CreateRequest> = Request::new(CreateRequest {
        id: "Tonic".into(),
        x_size: x_w,
        y_size: y_h,
    });

   let response: Response<MapInstance> = client.create_template(request).await?;
   return Ok(response.into_inner());
}

pub async fn get_map(id: String) -> Result<MapInstance, Box<dyn Error>> {
	debug!("Requested create map.");

    let url = get_url();

    debug!("The connection url is {}.", url);
    let url_formated: String = format!("http://{}:50051", url); 
    let mut client = MapClient::connect(url_formated).await?;

    let request: Request<GetRequest> = Request::new(GetRequest {
        id: id,
    });

   let response: Response<MapInstance> = client.get(request).await?;
   return Ok(response.into_inner());
}